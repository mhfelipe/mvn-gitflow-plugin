package com.example.modulo1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculoTest {

    @Test
    public void testSoma() {
        assertEquals(5, calculo.soma(2, 3));
    }

    private final Calculo calculo = new Calculo();

}