package com.example.modulo1;

public class App {

    public void bugfixRelease() {
        System.out.println("Bug fix release.");
    }

    public void hotfix() {
        System.out.println("Hotfix");
    }

    public static void main(String[] args) {
        System.out.println("Primeiro desenvolvimento.");
    }

}
